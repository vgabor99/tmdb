package com.vargag99.MovieSearchResponsetmdb.di

import com.vargag99.tmdb.api.ITmdbApiService
import com.vargag99.tmdb.repo.ITmdbRepo
import com.vargag99.tmdb.repo.TmdbRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule {
    @Provides
    @Singleton
    internal fun provideRepo(tmdbApiService: ITmdbApiService): ITmdbRepo {
        return TmdbRepo(tmdbApiService)
    }

}

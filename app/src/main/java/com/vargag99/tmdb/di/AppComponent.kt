package com.vargag99.MovieSearchResponsetmdb.di

import com.vargag99.tmdb.TmdbApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        AppModule::class
    ]
)
@Singleton
internal interface AppComponent : AndroidInjector<TmdbApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<TmdbApplication>()
}
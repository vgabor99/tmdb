package com.vargag99.MovieSearchResponsetmdb.di

import com.vargag99.tmdb.di.ApiModule
import dagger.Module

@Module(
    includes = [
        ApiModule::class,
        RepoModule::class,
        ViewModelModule::class
    ]
)
object AppModule {
}
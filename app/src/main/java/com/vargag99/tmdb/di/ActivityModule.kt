package com.vargag99.MovieSearchResponsetmdb.di

import com.vargag99.tmdb.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    // TODO other activities here
}
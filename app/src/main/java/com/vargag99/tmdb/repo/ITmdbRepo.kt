package com.vargag99.tmdb.repo

import io.reactivex.Observable

interface ITmdbRepo {
    fun searchMovies(query: String): Observable<List<Movie>>

}
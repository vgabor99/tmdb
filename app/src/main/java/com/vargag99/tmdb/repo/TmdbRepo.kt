package com.vargag99.tmdb.repo

import com.vargag99.tmdb.api.ITmdbApiService
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TmdbRepo @Inject constructor(private val tmdbApiService: ITmdbApiService) : ITmdbRepo {
    override fun searchMovies(query: String): Observable<List<Movie>> {
        return tmdbApiService.searchMovies(query)
            .map { response ->
                response.results.map { movieFromApiData(it) }
            }
            .subscribeOn(Schedulers.io())

    }

    // TODO fund a better place for the API <--> UI model converters
    // I did not add a secondary constructor to Movie, as I did not want to pollute it with API details
    private fun movieFromApiData(apiData: com.vargag99.tmdb.api.Movie): Movie = Movie(
        id = apiData.id,
        title = apiData.title,
        posterUri = "http://image.tmdb.org/t/p/w185/${apiData.poster_path}"
    )

}

package com.vargag99.tmdb.repo

// UI model class
data class Movie(
    val id: Integer,
    val title: String,
    val posterUri: String
)

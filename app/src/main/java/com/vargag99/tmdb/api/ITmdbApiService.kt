package com.vargag99.tmdb.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ITmdbApiService {

    @GET("search/movie/")
    fun searchMovies(
        @Query("query") query: String
    ):
            Observable<MovieSearchResponse> // TODO Single
}

package com.vargag99.tmdb

import com.vargag99.MovieSearchResponsetmdb.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class TmdbApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<TmdbApplication> {
        return DaggerAppComponent.builder().create(this)
    }

}

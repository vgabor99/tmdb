package com.vargag99.tmdb.ui.movielist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.vargag99.tmdb.BR
import com.vargag99.tmdb.R
import com.vargag99.tmdb.databinding.MovieListBinding
import com.vargag99.tmdb.repo.Movie
import dagger.android.support.DaggerFragment
import me.tatarka.bindingcollectionadapter2.ItemBinding
import javax.inject.Inject

class MovieListFragment : DaggerFragment() {

    companion object {
        fun newInstance() = MovieListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MovieListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: MovieListBinding =
            DataBindingUtil.inflate(inflater, R.layout.movie_list, container, false)
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel
        binding.movieItemBinding = ItemBinding.of<Movie>(BR.item, R.layout.movie_list_item)
        viewModel.errors.observe(this, Observer { error ->
            error?.let { Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show() }
        })
        return binding.root

        return inflater.inflate(R.layout.movie_list, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieListViewModel::class.java)
    }

}

package com.vargag99.tmdb.ui.movielist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vargag99.tmdb.generic.SingleLiveEvent
import com.vargag99.tmdb.repo.ITmdbRepo
import com.vargag99.tmdb.repo.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MovieListViewModel @Inject constructor(private val tmdbRepo: ITmdbRepo) : ViewModel() {
    private val disposables = CompositeDisposable()

    private lateinit var moviesVar: MutableLiveData<List<Movie>>

    val movies: LiveData<List<Movie>>
        get() {
            if (!::moviesVar.isInitialized) {
                moviesVar = MutableLiveData()
                loadData()
            }
            return moviesVar
        }

    val errors =
        SingleLiveEvent<Throwable>() // TODO SingleLiveEvent is a kind of hack, still better than no error handling.

    val query = MutableLiveData<String>()

    private val progressVar = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean> = progressVar

    private fun handleResult(result: List<Movie>) {
        moviesVar.value = result
    }

    private fun handleError(error: Throwable) {
        errors.value = error
    }

    private fun loadData() {
        val queryRx = BehaviorSubject.create<String>()
        query.observeForever { queryRx.onNext(it) }
        disposables.add(queryRx
            .throttleWithTimeout(500, TimeUnit.MILLISECONDS)
            .doOnNext { progressVar.postValue(true) }
            .switchMap { q ->
                tmdbRepo.searchMovies(query = q)
                    .onErrorReturnItem(arrayListOf())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { progressVar.postValue(false) }
            .subscribe(
                { result -> handleResult(result) },
                { error -> handleError(error) }
            ))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}
